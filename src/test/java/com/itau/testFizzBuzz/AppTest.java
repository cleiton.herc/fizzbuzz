package com.itau.testFizzBuzz;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */

    public void testApp1()
    {
    	FizzBuzz fizzBuzz = new FizzBuzz();
    	String retorno = fizzBuzz.validaNumeros(1);
    	System.out.println( "Teste entrada 01: " +  retorno );
    	assertEquals(retorno, "1");
    }
    
    public void testApp3()
    {
    	FizzBuzz fizzBuzz = new FizzBuzz();
    	String retorno = fizzBuzz.validaNumeros(3);
    	System.out.println( "Teste entrada 03: " + retorno );
    	assertEquals( retorno , "12Fizz" );
    }
    
    public void testApp5()
    {
    	FizzBuzz fizzBuzz = new FizzBuzz();
    	String retorno = fizzBuzz.validaNumeros(5);
    	System.out.println( "Teste entrada 05: " +  retorno );
    	assertEquals( retorno , "12Fizz4Buzz" );
    }
    
    public void testApp15()
    {
    	FizzBuzz fizzBuzz = new FizzBuzz();
    	String retorno = fizzBuzz.validaNumeros(15);
    	System.out.println( "Teste entrada 15: " + retorno );
    	assertEquals(  retorno , "12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz" );
    }
}
